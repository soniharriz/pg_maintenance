#!/bin/bash
#script to iterate through bloated table and vacuum full or cluster them where size is quite small, while table is not locked.

set -e

declare -a process_type=$1

CLUSTER_LIST=/var/lib/pgsql/soniharriz/files/cluster_list
VACUUM_FULL_LIST=/var/lib/pgsql/soniharriz/files/vacuum_full_list
VACUUM_LIST=/var/lib/pgsql/soniharriz/files/vacuum_list
REINDEX_LIST=/var/lib/pgsql/soniharriz/files/reindex_list
DB_LIST=/var/lib/pgsql/soniharriz/files/db_list
EXCLUDED_SCHEMA="'_consprod_replication','partman','repack'"
#SCHEMA="'descartes','donlen','excavator','public','risk','screening','translore'"
#DBNAME=consprod
SIZE_LIMIT=209715200 # table size limit, in bytes
XID_AGE_LIMIT=150000000
#SIZE_LIMIT=5073741824

#MAIL_TO=soniharriz@usicllc.com
LAST_EXECUTION_DAY=tomorrow # valid items are : today, tomorrow, day name, next-week, next-month, next-year as of valid string time for 'date' command. This parameter is to anticipate command that last through next day.
LAST_EXECUTION_TIME=04:00:00 # on 001 it should be before 4 AM, on 002 it should be before midnight
MAX_CONCURRENT_XACT=5
LONGEST_XACT="'1 minutes'"
PGOPTIONS="--maintenance_work_mem=256MB"

# Listing tables to be clustered or vacuum full

function list_vacuum {
  echo `date` "LOG: Listing tables for vacuum"
  psql -A -t -v ON_ERROR_STOP=1 -c "select * from (select relid as relid from pg_stat_user_tables where pg_total_relation_size(relid) > 100*1024*1024 and n_live_tup > 0 and ((n_dead_tup > 1000000 and 100*n_dead_tup/n_live_tup >= 5) or 100*n_dead_tup/n_live_tup > 10) UNION select c.oid as relid from pg_class c join pg_namespace n on c.relnamespace = n.oid WHERE c.relkind = 'r' and pg_total_relation_size(c.oid) > 1024*1024*1024 and age(c.relfrozenxid) >= $XID_AGE_LIMIT) a order by pg_total_relation_size(relid) desc;" $DBNAME > $VACUUM_LIST
}

function list_cluster {
  echo `date` "LOG: Listing tables for cluster"
  psql -A -t -v ON_ERROR_STOP=1 -c "select c.oid from table_bloat tb join pg_namespace ns on tb.schemaname = ns.nspname join pg_class c on ns.oid = c.relnamespace and tb.tblname = c.relname join pg_index i ON i.indrelid = c.oid where not tb.is_na and relkind = 'r' AND relhasindex AND i.indisclustered and bloat_ratio >= 20 and pg_total_relation_size(c.oid) <= $SIZE_LIMIT and schemaname in ($SCHEMA) order by bloat_ratio desc;" $DBNAME > $CLUSTER_LIST
}

function list_vacuum_full {
  echo `date` "LOG: Listing tables for vacuum full"
  psql -A -t -v ON_ERROR_STOP=1 -c "select c.oid from table_bloat tb join pg_namespace ns on tb.schemaname = ns.nspname join pg_class c on ns.oid = c.relnamespace and tb.tblname = c.relname where not tb.is_na and tb.schemaname || '.' || tb.tblname not in (select tb.schemaname || '.' || tb.tblname from table_bloat tb join pg_namespace ns on tb.schemaname = ns.nspname join pg_class c on ns.oid = c.relnamespace and tb.tblname = c.relname join pg_index i ON i.indrelid = c.oid where not tb.is_na and relkind = 'r' AND relhasindex AND i.indisclustered and bloat_ratio >= 20 and pg_total_relation_size(c.oid) <= $SIZE_LIMIT and schemaname in ($SCHEMA)) and bloat_ratio >= 20 and pg_total_relation_size(c.oid) <= $SIZE_LIMIT and schemaname in ($SCHEMA) order by bloat_ratio desc;" $DBNAME > $VACUUM_FULL_LIST
}

function list_reindex {
  echo `date` "LOG: Listing indexes for reindex"
  psql -A -t -v ON_ERROR_STOP=1 -c "select ui.indexrelid from btree_bloat bb join pg_stat_user_indexes ui on bb.schemaname = ui.schemaname and bb.tblname = ui.relname and bb.idxname = ui.indexrelname where not bb.is_na and bb.bloat_ratio >= 20 and pg_relation_size(ui.relid) <= $SIZE_LIMIT and bb.schemaname in ($SCHEMA) order by ui.relname;" $DBNAME > $REINDEX_LIST
}

function get_now_epoch {
  now_epoch=`date +%s`
}

function get_lock_num {
  if [ "$process_type" == "vacuum_full" ] || [ "$process_type" == "cluster" ]; then
    lock_num=`psql -t -A -c "select count(*) from pg_locks where relation = $relid;" $DBNAME`
  elif [ "$process_type" == "vacuum" ]; then
    lock_num=0
  elif [ "$process_type" == "reindex" ]; then
    tableid=`psql -t -A -c "select relid from pg_stat_user_indexes where indexrelid = $relid" $DBNAME`
    lock_num=`psql -t -A -c "select count(*) from pg_locks where relation = $relid;" $DBNAME`
  fi
}

function get_relid {
  relid=`head -1 $list`
  sed -i '1d' $list
}

function get_concurrent_xact {
  concurrent_xact=`psql -t -A -c "select count(*) from pg_stat_activity where state != 'idle' and query not ilike '%autovacuum%';" $DBNAME`
}

function get_long_xact {
  long_xact=`psql -t -A -c "select count(*) from pg_stat_activity where state != 'idle' and query not ilike '%autovacuum%' and now()-xact_start >= interval $LONGEST_XACT ;" $DBNAME`
}

#Loop start

psql -A -t -v ON_ERROR_STOP=1 -c "select datname from pg_database where datistemplate=false;" postgres > $DB_LIST

while read DBNAME
do
  echo "now processing $DBNAME"
  SCHEMA=`psql -A -t -v ON_ERROR_STOP=1 -c "
    SELECT array_to_string (ARRAY(
    SELECT '''' || n.nspname || ''''
    FROM pg_catalog.pg_namespace n
    WHERE n.nspname !~ '^pg_' AND n.nspname <> 'information_schema'
    and n.nspname not in ($EXCLUDED_SCHEMA)
    ORDER BY 1),',');" $DBNAME`
  echo $SCHEMA

  last_execution_day_date=`date -d $LAST_EXECUTION_DAY +%F`
  last_execution_time_epoch=`date -d "$last_execution_day_date $LAST_EXECUTION_TIME" +%s`

  # Iteration through each tables to be clustered

  if [ "$process_type" == "cluster" ]; then
    list_cluster
    list=$CLUSTER_LIST
    command='cluster'
    command_text='cluster'
  elif [ "$process_type" == "vacuum_full" ]; then
    list_vacuum_full
    list=$VACUUM_FULL_LIST
    command='vacuum full'
    command_text='vacuum full'
  elif [ "$process_type" == "vacuum" ]; then
    list_vacuum
    list=$VACUUM_LIST
    command='vacuum'
    command_text='vacuum'
  elif [ "$process_type" == "reindex" ]; then
    list_reindex
    list=$REINDEX_LIST
    command='reindex index'
    command_text='reindex'
  else
    echo `date` "ERROR: Invalid parameter $1"
    echo "   "
    exit 1
  fi

  echo `date` "LOG: List is $list and command is $command_text"

  max=`wc -l < $list`
  echo `date` "LOG: There are $max tables/indexes for $command_text"

  counter=0
  get_relid

  while [ $counter -lt $max ]; do

    relname=`psql -t -A -c "select ns.nspname || '.' || c.relname from pg_class c join pg_namespace ns on ns.oid = c.relnamespace where c.oid = $relid;" $DBNAME`
    echo `date` "LOG: Preparing to $command_text table/index $relname"
    get_now_epoch
    get_lock_num
    get_concurrent_xact
    get_long_xact

    while [ $concurrent_xact -gt $MAX_CONCURRENT_XACT ] || [ $long_xact -gt 0 ] || [ $lock_num -gt 0 ] || [ $now_epoch -gt $last_execution_time_epoch ]; do
      if [ $now_epoch -gt $last_execution_time_epoch ]; then
        echo `date` "ERROR: Outside execution time"
        echo "  "
        exit 1
      elif [ $concurrent_xact -gt $MAX_CONCURRENT_XACT ] || [ $long_xact -gt 0 ]; then
        echo `date` "LOG: Sleep for 5 minutes due to high number of concurrent transaction or long running transaction occur"
        sleep 300
      else
        echo `date` "LOG: Sleep for 10 seconds due to table still locked"
        sleep 10
      fi
      get_now_epoch
      get_lock_num
      get_concurrent_xact
      get_long_xact
    done

    echo `date` "LOG: Execution ready. SQL commands are '$command $relname;'"
    PGOPTIONS="$PGOPTIONS" psql -d $DBNAME <<EOF
      \timing on
      $command $relname;
EOF
    if [ "$process_type" == "vacuum_full" ] || [ "$process_type" == "cluster" ] || [ "$process_type" == "vacuum" ]; then
      PGOPTIONS="$PGOPTIONS" psql -d $DBNAME <<EOF
        \timing on
        analyze $relname;
EOF
    fi
    let counter=counter+1
    get_relid

  done
#Loops End
done < $DB_LIST


echo `date` "LOG: Scripts done"
echo "   "

